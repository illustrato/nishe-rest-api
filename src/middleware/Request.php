<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Middleware
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Nishe\Middleware;

use Phalcon\Events\Event;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

use Nishe\Plugin\Http\Response;

use Nishe\Traits\Response as ResponseTrait;
use Nishe\Traits\Token as TokenTrait;

/**
 * RequestMiddleware class
 *
 * @category  PHP
 * @package   Nishe\Middleware
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 *
 * @property Request  $request
 * @property Response $response
 */
class Request implements MiddlewareInterface
{
    use ResponseTrait;
    use TokenTrait;
    /**
     * Execute before the router so we can determine if this is a private controller,
     * and must be authenticated, or a public controller that is open to all.
     *
     * @param Event $event
     * @param Micro $app
     *
     * @return bool
     */
    public function beforeExecuteRoute(Event $event, Micro $api): bool
    {
        /** @var Request $request */
        $request = $api->getService('request');
        /** @var Response $response */
        $response = $api->getService('response');

        $dispatcher = explode('/', $request->getURI());
        $controller = $dispatcher[1];
        $action = $dispatcher[2];
        $method = $request->getMethod();

        // Only check permissions on private controllers
        if ($method !== 'OPTIONS' && $api->acl->isPrivate($controller, $action)) {
            // If there is no token available, the route cancels
            if ($request->isEmptyBearerToken()) {
                $this->halt($api, $response::UNAUTHORIZED, 'No Bearer Token found in Header');
                return false;
            }

            // Verify Token
            if (($uId = $api->auth->uidByToken($request->getBearerToken())) === false) {
                $error = $api->session->get('XET');
                $this->halt($api, $response::BAD_REQUEST, $error->message, $error->code);
                return false;
            }

            // Check if the user have permission to the current option
            $profile = $api->auth->getProfile($uId, $request->getTenant())->name;
            if (!$api->acl->isAllowed($profile, $controller, $action)) {
                $this->halt($api, $response::FORBIDDEN, "'$profile' profile denied access to action '$controller:$action'");
                return false;
            }
            $api->session->set('UID', $uId);
        }
        return true;
    }

    /**
     * @param Micro $app
     *
     * @return bool
     */
    public function call(Micro $app)
    {
        return true;
    }
}
?>
