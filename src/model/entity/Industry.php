<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Model\Entity
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Nishe\Model\Entity;

use Nishe\Model\Base;
use function Nishe\Core\boolProof;

/**
 * Industry class
 *
 * @category  PHP
 * @package   Nishe\Model\Entity
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Industry extends Base
{
    /**
     * @param array $param = []
     * @param string $table = 'tenant'
     */
    public function __construct(array $param = [], string $table = 'industry')
    {
        parent::__construct($param);
        $this->source = $table;
    }

    /**
     * All Records
     *
     * @return array
     */
    public static function ls(string $target): array
    {
        $list = [];
        switch ($target) {
            case 'industry':
                $list = self::dql(
                    'SELECT i.*, phiscal.trademark(`TENANT_ID`) business FROM industry i LEFT JOIN tenant_industry USING(`CATEGORY_ID`,`COUNT`)'
                );
                $temp = [];
                foreach ($list as $item) {
                    $id = $item->CATEGORY_ID;
                    unset($item->CATEGORY_ID);
                    $item->ALT = $item->ALT ?? $item->NAME;
                    $temp[$id][intval($item->COUNT) - 1] = $item;
                }
                $list = $temp;
                break;
            default:
                $list = self::dql("SELECT * FROM $target");
                break;
        }
        return $list;
    }

    /**
     * Get Industries associated with a Tenant
     *
     * @return array
     */
    public static function getTenantIndustries(int $tenantId): array
    {
        $fields =
            'banner(e.tenant_id, e.category_id, e.count) file, ' .
            'e.tenant_id, e.category_id, e.count, i.name, i.alt, e.description, e.main, e.banner';
        return self::dql(
            "SELECT $fields FROM industry i INNER JOIN tenant_industry e USING (category_id, count) WHERE tenant_id = :tenant",
            [
                'tenant' => $tenantId,
            ]
        );
    }

    /**
     * Assign Industry to Entity
     *
     * @param int $yenantId
     * @param array &$industries
     *
     * @return void
     */
    public function associate(int $tenantId, array &$industries)
    {
        $param = [
            'tenant_id' => $tenantId,
            'category_id' => $this->category_id,
            'count' => intval($this->count),
            'main' => boolProof($this->main ?? -1),
            'description' => $this->description ?? null,
            'banner' => $this->banner ?? null,
        ];
        $model = self::storedProcedure('associate_industry', $param);
        if ($model === false) {
            return false;
        }
        $industries = $model->getStatement()->fetchAll(\PDO::FETCH_CLASS, self::class);
        return $model->verifySP();
    }
}
?>
