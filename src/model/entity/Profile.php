<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Model\Entity
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Nishe\Model\Entity;

use Nishe\Model\Base;
/**
 * Profile class
 *
 * @category  PHP
 * @package   Nishe\Model\Entity
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Profile extends Base
{
    /**
     * Returns table name mapped in the model
     *
     * @param string $source	: Database Table/View
     *
     * @return string
     */
    public function getSource(): string
    {
        return 'phiscal.profile';
    }

    /**
     * Profile List function
     *
     * @return array
     */
    public static function ls(): array
    {
        return self::dql('SELECT PROFILE_ID id, name, description FROM phiscal.profile', null, \PDO::FETCH_CLASS, self::class);
    }

    /**
     * Returns the action description according to its simplified name
     *
     * @param string $action
     *
     * @return string
     */
    public static function getActionDescription(string $action)
    {
        $sql = 'SELECT description FROM action WHERE NAME = :action';
        return self::record($sql, ['action' => $action])->description ?? false;
    }

    /**
     * List all available Permissions
     *
     * @return array
     */
    public function getPermissions(): array
    {
        return self::dql('SELECT * FROM v_profilePermission WHERE id = :id', ['id' => $this->id]);
    }

    /**
     * Verify Token with Database
     *
     * @param string $token
     *
     * @return string
     */
    public static function verifyToken(string $token)
    {
        $sql = 'SELECT user_id FROM phiscal.token WHERE TOKEN = :token';
        return self::record($sql, ['token' => $token])->user_id ?? false;
    }

    /**
     * User Profiles
     *
     * @param string $uid
     *
     * @return array
     */
    public static function getList(string $uid): array
    {
        $result = self::storedfunction('phiscal.userprofiles', ['user_id' => $uid]);
        return $result === false ? [] : json_decode($result);
    }

    /**
     * User Profile with Tenant
     *
     * @param int $tenantId
     *
     * @return false|object
     */
    public function onTenant(int $tenantId)
    {
        $param = ['user_id' => $this->user_id, 'tenant_id' => $tenantId];
        $result = self::storedfunction('phiscal.userprofile', $param);
        return $result === false ? false : json_decode($result);
    }
}
?>
