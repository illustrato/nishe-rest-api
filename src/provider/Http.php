<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Provider
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Nishe\Provider;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

use Phalcon\Http\Response\Cookies;
use Nishe\Plugin\Http\Response;
use Nishe\Plugin\Http\Request;

/**
 * Response class
 *
 * @category  PHP
 * @package   Nishe\Provider
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Http implements ServiceProviderInterface
{
    /**
     * @param DiInterface $di
     *
     * @return void
     */
    public function register(DiInterface $di): void
    {
        $di->set('cookies', function () {
            return new Cookies(true, "#1dj8$=dp?.ak//j1V$~%*0XaK\xb1\x8d\xa9\x98\x054t7w!z%C*F-Jk\x98\x05\\\x5c");
        });
        $di->setShared('request', new Request());
        $di->setShared('response', function () {
            $response = new Response();
            $response->setSession($this->getShared('session'));
            $response
                ->setCookies($this->getShared('cookies'))
                ->setStatusCode(200) //Assume success. We will work with the edge cases in the code
                ->setContentType('application/vnd.api+json', 'UTF-8');
            return $response;
        });
    }
}
?>
