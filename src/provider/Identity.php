<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Provider
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Nishe\Provider;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

use Nishe\Plugin\Identity\Acl;
use Nishe\Plugin\Identity\Auth;
use Nishe\Model\Entity\Index;
/**
 * Acl class
 *
 * @category  PHP
 * @package   Nishe\Provider
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Identity implements ServiceProviderInterface
{
    /**
     * @param DiInterface $di
     *
     * @return void
     */
    public function register(DiInterface $di): void
    {
        $di->setShared('acl', function () {
            $acl = new Acl();
            $acl->addPrivateResources(Index::acl($acl));
            return $acl;
        });
        $di->setShared('auth', Auth::class);
    }
}
?>
