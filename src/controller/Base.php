<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Controller
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Nishe\Controller;

use Phalcon\Mvc\Controller as Control;
use Phalcon\Validation;
use Phalcon\Messages\Message;
use Nishe\Model\Entity\Index as Model;
use function Nishe\Core\appPath;
/**
 * Base class
 *
 * @category  PHP
 * @package   Nishe\Controller
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
abstract class Base extends Control
{
    /**
     * Preflighter
     * @method OPTIONS
     *
     * @return void
     */
    public function preflight(): void
    {
        $this->response->setStatusCode(204, 'No Content')->send();
    }

    /**
     * Cut Execution short if Email feature is Disabled
     *
     * @return void
     */
    public function checkEmailConfig(): void
    {
        if (!$this->config->app->useMail) {
            $this->response
                ->setPayloadSuccess('Emails are currently disabled. Change config key "useMail" to true to enable emails')
                ->setStatusCode($this->response::CREATED)
                ->send();
            exit();
        }
    }

    /**
     * Attempt Sending Email
     *
     * @param array $email	: Email Information
     * @param object $model	: Record to delete on sending fail
     *
     * @return void
     */
    public function sendMail(array $email, $model = false): void
    {
        $error = '';
        if (!$this->mail->send($email['to'], $email['subject'], $email['message'], $error)) {
            if ($model !== false) {
                $model->delete();
            }
            $this->intercept($error, $this->response::BAD_REQUEST);
        }
    }

    /**
     * Kills Application on bad Request
     *
     * @return void
     */
    public function checkDbRequest(): void
    {
        if ($this->session->has('XET')) {
            $errors = $this->session->get('XET');
            $messages = [];
            $string = '';
            $length = count($errors->messages);
            for ($i = 0; $i < $length; $i++) {
                $message = $messages[] = $errors->messages[$i];
                $string .= "{$message}" . $i + 1 === $length ? '' : ';';
            }
            $this->response
                ->reportErrors($messages, $errors->code)
                ->setStatusCode($this->response::INTERNAL_SERVER_ERROR)
                ->send();
            Model::notify($this->session->get('UID') ?? '0', 'log', 'fa fa-database text-danger', $string);
            exit();
        }
    }

    /**
     * Kills Application on incorrect Request Method
     *
     * @param mixed $result	: Target Object
     * @param string $message	: Custom Error Message
     *
     * @return void
     */
    public function checkRecord($result, string $message = 'Record Missing', int $code = 404): void
    {
        if ($result === false) {
            $this->intercept($message, $code);
        }
    }

    /**
     * Kills Application on incorrect Request Method
     *
     * @param string $method	: Desired Method in Pascal casing
     *
     * @return void
     */
    public function checkRequestMethod(string $method): void
    {
        if (!$this->request->{"is$method"}()) {
            $this->intercept("$method Resquest", $this->response::BAD_REQUEST);
        }
        if (file_exists(appPath('var/cache/session/state-tmp'))) {
            unlink(appPath('var/cache/session/state-tmp'));
        }
    }

    /**
     * Kills Application on invalid Input
     *
     * @param array $data		: User Input
     * @param Validation $Validator	: Validation Logic
     *
     * @return void
     */
    public function validateInput(array $data, Validation $validator): void
    {
        $messages = $validator->validate($data);
        if (count($messages) > 0) {
            $this->response
                ->reportErrors($messages)
                ->setStatusCode($this->response::FORBIDDEN)
                ->send();
            exit();
        }
    }

    /**
     * Create Directory
     *
     * @param string $dir	: target directory
     *
     * @return string
     */
    public function healthyPath(string $dir): string
    {
        $path = "{$this->config->app->files}$dir";
        if (!is_dir($path) && !mkdir($path, 0777, true)) {
            $this->intercept("Couldn't create Directory '$path'", $this->response::FORBIDDEN);
        }
        if (!is_writable($path)) {
            $this->intercept("Unable to write path '$path'", $this->response::FORBIDDEN);
        }

        return $path;
    }

    /**
     * create file
     *
     * @param string $dir	: target file path
     * @param string $name	: target file name
     * @param object $param	: File Info
     *
     * @return false|int
     */
    public function touch(string $dir, string $name, array $param)
    {
        $path = $this->healthyPath($dir);
        $file = "$path/$name.{$param['ext']}";
        $blob = base64_decode(explode(',', $param['base64'])[1], true);
        if ($blob === false) {
            $this->intercept('File Corrupt', $this->response::BAD_REQUEST);
        }
        $bytes = file_put_contents($file, $blob);
        if ($bytes === false) {
            $this->intercept("Couldn 't create file '$file'", $this->response::FORBIDDEN);
        }
        return $bytes;
    }

    /**
     * Cancel Resquest at that point with message
     *
     * @param string	$message
     * @param int	$code
     *
     * @return void
     */
    public function intercept(string $message, int $httpCode, int $errorCode = null, string $icon = null, $model = false): void
    {
        if ($httpCode >= 200 && $httpCode < 300) {
            $this->response
                ->setPayloadSuccess($message)
                ->setStatusCode($httpCode)
                ->send();
        } else {
            if (!empty($icon)) {
                Model::notify($this->session->get('UID') ?? '0', 'log', $icon, null, $message);
            }
            if ($model !== false) {
                $model->delete();
            }
            $this->response
                ->reportErrors([$message], $errorCode)
                ->setStatusCode($httpCode)
                ->send();
        }
        exit();
    }
}
?>
