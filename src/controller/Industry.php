<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Controller
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Nishe\Controller;

use Nishe\Model\Entity\Index;
use Nishe\Model\Entity\Industry as Model;

/**
 * Industry class
 *
 * @category  PHP
 * @package   Nishe\Controller
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Industry extends Base
{
    /**
     * List All Industries
     * PUBLIC
     * DB Requests :1
     *
     * @param int $tenantId
     *
     * @method GET
     */
    public function ls(string $target, int $tenantId): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Get');

        // make HTTP response
        switch ($target) {
            case 'this':
                switch ($tenantId) {
                    case 0:
                        $this->response->setPayloadSuccess(Model::ls('industry'))->send();
                        break;
                    default:
                        $this->response->setPayloadSuccess(Model::getTenantIndustries($tenantId))->send();
                        break;
                }
                break;
            case 'category':
                $this->response->setPayloadSuccess(Model::ls('category'))->send();
                break;
        }
    }

    /**
     * Get User Data
     * PRIVATE
     * DB Requests :1
     *
     * @method GET
     */
    public function getData(string $categoryId, int $count)
    {
        // verify HTTP request method
        $this->checkRequestMethod('Get');

        $id = [
            'tenant_id' => $this->request->getTenant(),
            'category_id' => $categoryId,
            'count' => $count,
        ];

        // make HTTP response
        $fields = 'e.banner, e.tenant_id, e.category_id, e.count, i.name, i.alt, e.description, e.main';
        $source = 'industry i INNER JOIN tenant_industry e USING (category_id, count)';
        $this->response->setPayloadSuccess(Model::getData($id, $fields, $source))->send();
    }

    /**
     * Assign Industry to Entity
     * PRIVATE
     * DB Requests :1
     *
     * @method POST
     */
    public function associate(): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Post');

        $payload = (array) $this->request->getJsonRawBody();

        // Check for Business Banner
        $banner = ['up' => false];
        $useMain = $payload['picture']->useMain ?? false;
        if ($useMain === false && !empty($payload['picture']->base64)) {
            $banner = array_merge(['up' => true], (array) $payload['picture']);
            $payload['banner'] = $payload['picture']->ext;
        }
        unset($payload['picture']);
        if ($useMain) {
            $payload['banner'] = 'main';
        }

        // create/Modify record in Database
        $model = new Model($payload);
        $industries = [];
        $tenant = $model->associate($this->request->getTenant(), $industries);

        $this->checkDbRequest();

        if ($banner['up']) {
            $this->touch("img/banner/{$this->request->getTenant()}", "{$payload['category_id']}{$payload['count']}", $banner);
        }

        // make HTTP response
        $this->response->setPayloadSuccess($industries)->send();
    }

    /**
     * Dissassociate Industry from Entity
     * PRIVATE
     * DB Requests :2
     *
     * @param string $categoryId	: Category Id
     * @param int $count		: Industry Id
     *
     * @method DELETE
     */
    public function disconnect(string $categoryId, int $count): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Delete');

        $tenantId = $this->request->getTenant();
        $industry = Model::getData(
            ['category_id' => $categoryId, 'count' => $count, 'tenant_id' => $tenantId],
            'banner',
            'tenant_industry'
        );
        $this->checkRecord($industry->delete(), 'Process Failed', $this->response::INTERNAL_SERVER_ERROR);

        $file = "{$this->config->app->files}img/banner/{$tenantId}/{$categoryId}{$count}.{$industry->banner}";
        if ($industry->banner) {
            unlink($file);
        }

        // make HTTP response
        $this->response->setPayloadSuccess(Model::getTenantIndustries($tenantId))->send();
    }

    /**
     * Delete Entity Industry Banner
     * PRIVATE
     * DB Requests :1
     *
     * @param string $categoryId	: Category Id
     * @param int $count		: Industry Id
     *
     * @method DELETE
     */
    public function rmBanner(string $categoryId, int $count): void
    {
        // verify HTTP request method
        $this->checkRequestMethod('Delete');

        $tenantId = $this->request->getTenant();
        $industry = Model::getData(
            ['category_id' => $categoryId, 'count' => $count, 'tenant_id' => $tenantId],
            'banner',
            'tenant_industry'
        );
        $this->checkRecord($industry, 'Records Missing', $this->response::NOT_FOUND);
        if ($industry->update(['BANNER' => '?'])) {
            $file = "{$this->config->app->files}img/banner/{$tenantId}/{$categoryId}{$count}.{$industry->banner}";
            unlink($file);
        }

        // make HTTP response
        $this->response->setPayloadSuccess(Model::getTenantIndustries($tenantId))->send();
    }
}
?>
