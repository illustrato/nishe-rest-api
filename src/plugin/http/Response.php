<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Plugin\Http
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Nishe\Plugin\Http;

use Phalcon\Http\Response as PhResponse;
use Phalcon\Http\ResponseInterface;
use Phalcon\Messages\Message;
use Nishe\Model\Entity\Index as Model;
/**
 * Response class
 *
 * @category  PHP
 * @package   Nishe\Plugin\Http
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Response extends PhResponse
{
    const OK = 200;
    const CREATED = 201;
    const ACCEPTED = 202;
    const MOVED_PERMANENTLY = 301;
    const FOUND = 302;
    const TEMPORARY_REDIRECT = 307;
    const PERMANENTLY_REDIRECT = 308;
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const INTERNAL_SERVER_ERROR = 500;
    const NOT_IMPLEMENTED = 501;
    const BAD_GATEWAY = 502;

    private $codes = [
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        301 => 'Moved Permanently',
        302 => 'Found',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
    ];

    private $code;
    private $session;

    /**
     * Give Access to the Session variable
     *
     * @param SessionManager $session
     *
     * @return void
     */
    public function setSession($session): void
    {
        $this->session = $session;
    }

    /**
     * Returns the http code description or if not found the code itself
     * @param int $code
     *
     * @return int|string
     */
    public function getHttpCodeDescription(int $code)
    {
        if (true === isset($this->codes[$code])) {
            return sprintf('%s', $this->codes[$code]);
        }

        return $code;
    }

    /**
     * Sets the payload code as Errors
     *
     * @param array $errors
     *
     * @return self
     */
    public function reportErrors(array $errors, int $code = null): self
    {
        $this->setJsonContent(['errors' => $errors]);
        $this->code = $code;
        return $this;
    }

    /**
     * Sets the payload code as Success
     *
     * @param null|string|array $content The content
     *
     * @return Response
     */
    public function setPayloadSuccess($content = []): self
    {
        if ($this->session->has('XET')) {
            $errors = $this->session->get('XET');
            $messages = [];
            $string = '';
            $length = count($errors->messages);
            for ($i = 0; $i < $length; $i++) {
                $message = $messages[] = $errors->messages[$i];
                $string .= "{$message}" . $i + 1 === $length ? '' : ';';
            }
            $this->reportErrors($messages, $errors->code)->setStatusCode(self::INTERNAL_SERVER_ERROR);
            Model::notify($this->session->get('UID') ?? '0', 'log', 'fa fa-database text-danger', $string);
            return $this;
        }
        $data = true === is_array($content) ? $content : ['value' => $content];
        $data = true === isset($data['value']) ? $data : ['value' => $data];

        $this->setJsonContent($data);

        return $this;
    }

    /**
     * Send the response back
     *
     * @return ResponseInterface
     */
    public function send(): ResponseInterface
    {
        $content = empty($this->getContent())
            ? json_encode(['data' => $this->getHttpCodeDescription($this->getStatusCode())])
            : $this->getContent();
        $timestamp = date('c');
        $hash = sha1($timestamp . $content);
        $eTag = sha1($content);

        /** @var array $content */
        $content = json_decode($content, true);
        $jsonapi = ['jsonapi' => ['version' => '1.0']];
        $meta = ['meta' => ['timestamp' => $timestamp, 'hash' => $hash]];

        /*
         * Join the array again
         */
        $data = $jsonapi + $content + $meta;
        if ($this->code) {
            $data['code'] = $this->code;
        }
        $this->setHeader('E-Tag', $eTag)->setJsonContent($data);
        $this->session->destroy();
        return parent::send();
    }
}
?>
