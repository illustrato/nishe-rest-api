<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Plugin
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Nishe\Plugin;

use Phalcon\Di\Injectable;
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use function Nishe\Core\envValue;

class Mail extends Injectable
{
    /**
     * Sends e-mails based on predefined templates
     *
     * @param array  $to
     * @param string $subject
     * @param string $message
     *
     * @return bool
     */
    public function send(array $to, string $subject, string $message, &$error): bool
    {
        $config = $this->config->mail;
        //Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            /* $mail->SMTPDebug = SMTP::DEBUG_SERVER; //Enable verbose debug output */
            $mail->isSMTP(); //Send using SMTP
            $mail->Host = envValue('STRATO_SMTP_HOST', $config->smtp['host']); //Set the SMTP server to send through
            $mail->SMTPAuth = true; //Enable SMTP authentication
            $mail->CharSet = 'UTF-8';
            $mail->Username = envValue('STRATO_SMTP_USER', $config->smtp['address']); //SMTP username
            $mail->Password = envValue('STRATO_SMTP_PASSWD', $config->smtp['passwd']); //SMTP password
            $mail->addReplyTo('flames@illustrato.org', 'Information');
            $mail->SMTPSecure = $this->secure(envValue('STRATO_SMTP_SECURE', $config->smtp['secure'])); //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port = envValue('STRATO_SMTP_PORT', $config->smtp['port']); //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
            $mail->setFrom(envValue('STRATO_SMTP_USER', $config->smtp['address']), 'Nishe');
            $mail->isHTML(true);
            $mail->addAddress($to['email'], "{$to['recepient']}");
            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->send();
        } catch (Exception $e) {
            $error = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            return false;
        }
        return true;
    }

    /**
     * Securirity function
     *
     * @param string $param
     *
     * @return string
     */
    private function secure(string $param): string
    {
        return $param === 'STARTTLS ' ? PHPMailer::ENCRYPTION_STARTTLS : $param;
    }
}
?>
