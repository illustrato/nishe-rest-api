<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Plugin\Identity
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Nishe\Plugin\Identity;

use Phalcon\Acl\Adapter\AbstractAdapter;
use Phalcon\Di\Injectable;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Component as AclComponent;
use Phalcon\Acl\Enum as AclEnum;
use Phalcon\Acl\Role as AclRole;

use Nishe\Model\Entity\Profile;
use function Nishe\Core\appPath;

/**
 * Acl class
 *
 * @category  PHP
 * @package   Nishe\Plugin\Identity
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Acl extends Injectable
{
    const APC_CACHE_VARIABLE_KEY = 'nishe-acl';

    /**
     * The ACL Object
     *
     * @var AbstractAdapter|mixed
     */
    private $acl;

    /*
     * The file path of the ACL cache file.
     *
     * @var string
     */
    private $filePath;

    /**
     * Define the resources that are considered "private". These controller =>
     * actions require authentication.
     *
     * @var array
     */
    private $privateResources = [];

    /**
     * Stores private resource descriptions
     *
     * @var array
     */
    public $resourceDesc = [];

    /**
     * Checks if a controller is private or not
     *
     * @param string $controllerName
     *
     * @return bool
     */
    public function isPrivate(string $controller, string $action): bool
    {
        $controller = strtolower($controller);
        if (!isset($this->privateResources[$controller])) {
            return false;
        }
        $action = strtolower($action);
        return in_array($action, $this->privateResources[$controller]);
    }

    /**
     * Checks if the current profile is allowed to access a resource
     *
     * @param string $profile
     * @param string $controller
     * @param string $action
     *
     * @return bool
     */
    public function isAllowed(string $profile, string $controller, string $action): bool
    {
        return $this->getAcl()->isAllowed($profile, $controller, $action);
    }

    /**
     * Returns the ACL list
     *
     * @return AbstractAdapter|mixed
     */
    public function getAcl()
    {
        // Check if the ACL is already created
        if (is_object($this->acl)) {
            return $this->acl;
        }

        // Check if the ACL is in APC
        if (function_exists('apc_fetch')) {
            $acl = apc_fetch(self::APC_CACHE_VARIABLE_KEY);
            if ($acl !== false) {
                $this->acl = $acl;
                return $acl;
            }
        }
        $filePath = $this->getFilePath();

        // Check if the ACL is already generated
        if (!file_exists($filePath)) {
            $this->acl = $this->rebuild();
            return $this->acl;
        }

        // Get the ACL from the data file
        $data = file_get_contents($filePath);
        $this->acl = unserialize($data);

        // Store the ACL in APC
        if (function_exists('apc_store')) {
            apc_store(self::APC_CACHE_VARIABLE_KEY, $this->acl);
        }
        return $this->acl;
    }

    /**
     * Returns the permissions assigned to a profile
     *
     * @param Profile $profile
     *
     * @return array
     */
    public function getPermissions(Profile $profile): array
    {
        $permissions = [];
        foreach ($profile->getPermissions() as $permission) {
            $permissions["{$permission->resource}.{$permission->action}"] = true;
        }
        return $permissions;
    }

    /**
     * Returns all the resources and their actions available in the application
     *
     * @return array
     */
    public function getResources(): array
    {
        return $this->privateResources;
    }

    /**
     * Returns the action description according to its simplified name
     *
     * @param string $action
     *
     * @return string
     */
    public function getActionDescription(string $action)
    {
        return Profile::getActionDescription($action);
    }

    /**
     * Rebuilds the access list into a file
     *
     * @return AclMemory
     */
    public function rebuild(): AclMemory
    {
        // Activate Adapter
        $acl = new AclMemory();
        $acl->setDefaultAction(AclEnum::DENY);

        // Add Roles
        $profiles = Profile::ls();
        foreach ($profiles as $profile) {
            $acl->addRole(new AclRole($profile->name, $profile->description));
        }

        // Add Components
        foreach ($this->privateResources as $resource => $actions) {
            $acl->addComponent(new AclComponent($resource, $this->resourceDesc[$resource]), $actions);
        }

        // Grant access to private area to role Users
        foreach ($profiles as $profile) {
            // Grant permissions in "permissions" model
            foreach ($profile->getPermissions() as $permission) {
                $acl->allow($profile->name, $permission->resource, explode('/', $permission->action)[0]);
            }

            // Always grant these permissions
            $acl->allow($profile->name, 'industry', 'data');
        }

        $filePath = $this->getFilePath();
        if (touch($filePath) && is_writable($filePath)) {
            file_put_contents($filePath, serialize($acl));

            // Store the ACL in APC
            if (function_exists('apc_store')) {
                apc_store(self::APC_CACHE_VARIABLE_KEY, $acl);
            }
        } else {
            $this->session->set(
                'XET',
                (object) [
                    'message' => "The user does not have write permissions to create the ACL list at '$filePath'",
                    'code' => $this->response::FORBIDDEN,
                ]
            );
        }
        return $acl;
    }

    /**
     * Set the acl cache file path
     *
     * @return string
     */
    protected function getFilePath()
    {
        if (!isset($this->filePath)) {
            $path = rtrim(appPath($this->config->app->cacheDir), '\\/') . '/acl';
            if (!is_dir($path) && !mkdir($path, 0777, true)) {
                $this->response
                    ->setPayloadErrors("Couldn't create Directory '$path'")
                    ->setStatusCode($this->response::FORBIDDEN)
                    ->send();
                exit();
            }
            $this->filePath = "$path/data.txt";
        }
        return $this->filePath;
    }

    /**
     * Adds an array of private resources to the ACL object
     *
     * @param array $resources
     *
     * @return void
     */
    public function addPrivateResources(array $resources): void
    {
        if (empty($resources)) {
            return;
        }
        $this->privateResources = array_merge($this->privateResources, $resources);
        if (is_object($this->acl)) {
            $this->acl = $this->rebuild();
        }
    }
}
?>
