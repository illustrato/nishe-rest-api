<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Plugin\Identity
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */

namespace Nishe\Plugin\Identity;

use Phalcon\Di\Injectable;
use Phalcon\Security\JWT\Signer\Hmac;
use Phalcon\Security\JWT\Validator;
use Nishe\Model\Entity\Profile;
use Nishe\Traits\Token as TokenTrait;

/**
 * Auth class
 *
 * @category  PHP
 * @package   Nishe\Plugin\Identity
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
class Auth extends Injectable
{
    use TokenTrait;
    /**
     * Returns the current token user
     *
     * @param string $token
     *
     * @return false | User
     */
    public function uidByToken(string $token)
    {
        $uId = Profile::verifyToken($token);
        if ($uId === false) {
            $this->session->set('XET', (object) ['message' => 'The token isn\'t found in database', 'code' => 404]);
            return false;
        }
        try {
            // Parse the token received
            $tokenObject = $this->getToken($token);
            // Create the validator
            $validator = new Validator($tokenObject, 100); // allow for a time shift of 100

            $expires = $issued = time();
            $claims = $tokenObject->getClaims()->getPayload();
            $signer = new Hmac($this->getTokenAlgorithm());

            foreach ($claims['aud'] as $audience) {
                $validator->validateAudience($audience);
            }
            $validator
                ->validateExpiration($expires)
                ->validateId("{$claims['sub']}{$claims['iat']}")
                ->validateIssuedAt($issued)
                ->validateIssuer($claims['iss'])
                ->validateNotBefore($this->getTokenTimeNotBefore())
                ->validateSignature($signer, 'QcMpZ&' . sha1("{$claims['sub']}{$claims['iat']}"));
            return $uId;
        } catch (\Exception $e) {
            $this->session->set('XET', (object) ['messages' => [$e->getMessage()], 'code' => $e->getCode()]);
            return false;
        }
        return $check;
    }

    /**
     * Get Profiles associated with User ID
     *
     * @param string $uid
     *
     * @return array
     */
    public function getProfiles(string $uid): array
    {
        return Profile::getList($uid);
    }

    /**
     * User Profile
     *
     * @param string $uId
     * @param int $tenantId
     *
     * @return false|object
     */
    public function getProfile(string $uId, int $tenantId)
    {
        $model = new Profile(['user_id' => $uId]);
        return $model->onTenant($tenantId);
    }
}
?>
