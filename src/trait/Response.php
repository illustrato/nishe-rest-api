<?php
declare(strict_types=1);
/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * PHP Version 7.4
 *
 * @category  PHP
 * @package   Nishe\Traits
 * @author    Gowan Cephus <flames@illustrato.org>
 * @copyright 2021 Gowan Cephus
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt GPL3
 * @version   GIT: 2.27.0
 * @link      127.0.0.1
 */
namespace Nishe\Traits;

use Phalcon\Mvc\Micro;

/**
 * Trait Response
 * @author Gowan Cephus
 */
trait Response
{
    /**
     * Halt execution after setting the message in the response
     *
     * @param Micro  $api
     * @param int	$status
     * @param string	$message
     *
     * @return mixed
     */
    protected function halt(Micro $api, int $status, string $message, int $code = null)
    {
        /** @var Response $response */
        $response = $api->getService('response');
        $response
            ->setPayloadError($message, $code)
            ->setStatusCode($status)
            ->send();
        $api->stop();
    }
}
?>
