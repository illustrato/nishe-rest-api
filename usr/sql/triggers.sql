-- MariaDB dump 10.19  Distrib 10.5.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: phiscal
-- ------------------------------------------------------
-- Server version	10.5.13-MariaDB-0ubuntu0.21.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_insert_branch`
BEFORE INSERT ON `branch`
FOR EACH ROW
BEGIN

 DECLARE id		BIGINT UNSIGNED DEFAULT tenant(NEW.ORG_ID);
 DECLARE addr1		VARCHAR(45) DEFAULT TRIM(NEW.ADDRESSLINE1);
 DECLARE addr2		VARCHAR(45) DEFAULT TRIM(NEW.ADDRESSLINE2);
 DECLARE town		VARCHAR(45) DEFAULT TRIM(NEW.CITY);
 DECLARE nation		CHAR(45) DEFAULT UPPER(TRIM(NEW.COUNTRY));
 DECLARE geo		TEXT DEFAULT TRIM(NEW.GPS);
 DECLARE tel		CHAR(15) DEFAULT LOWER(TRIM(NEW.TELEPHONE));
 DECLARE info		VARCHAR(45) DEFAULT TRIM(NEW.DESCRIPTION);
 DECLARE pic		VARCHAR(5) DEFAULT LOWER(TRIM(NEW.PHOTO));

 DECLARE FOUND BOOLEAN DEFAULT TRUE;
 DECLARE COUNT TINYINT UNSIGNED DEFAULT (SELECT COUNT(*) FROM branch WHERE ORG_ID = id);
 DECLARE INCR MEDIUMINT UNSIGNED DEFAULT 1;

 WHILE FOUND DO
  SET NEW.BRANCH_NO = INCR;
  SET @BRANCH_NO = INCR;
  SET FOUND = ((SELECT COUNT(BRANCH_NO) FROM branch WHERE BRANCH_NO = NEW.BRANCH_NO AND ORG_ID = id) > 0);
  SET INCR = INCR + 1;
 END WHILE;


 SET @ORG_ID = id;
 SET NEW.ORG_ID = id;
 SET NEW.ADDRESSLINE1 = IF(addr1 = '' || addr1 = '?', \N, addr1);
 SET NEW.ADDRESSLINE2 = IF(addr2 = '' || addr2 = '?', \N, addr2);
 SET NEW.CITY = IF(town = '' || town = '?', \N, town);
 SET NEW.COUNTRY = IF(nation = '' || nation = '?', \N, nation);
 SET NEW.GPS = IF(geo = '' || geo = '?', \N, geo);
 SET NEW.DESCRIPTION = IF(info = '' || info = '?', \N, info);
 SET NEW.TELEPHONE = IF(tel = '' || tel = '?', \N, tel);
 SET NEW.PHOTO = IF(pic = '' || pic = '?', \N, pic);
 SET NEW.MAIN = COUNT < 1;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_update_branch`
BEFORE UPDATE ON `branch`
FOR EACH ROW
BEGIN

 DECLARE addr1		VARCHAR(45) DEFAULT TRIM(NEW.ADDRESSLINE1);
 DECLARE addr2		VARCHAR(45) DEFAULT TRIM(NEW.ADDRESSLINE2);
 DECLARE town		VARCHAR(45) DEFAULT TRIM(NEW.CITY);
 DECLARE nation		CHAR(45) DEFAULT UPPER(TRIM(NEW.COUNTRY));
 DECLARE geo		TEXT DEFAULT TRIM(NEW.GPS);
 DECLARE tel		CHAR(15) DEFAULT LOWER(TRIM(NEW.TELEPHONE));
 DECLARE info		VARCHAR(45) DEFAULT TRIM(NEW.DESCRIPTION);
 DECLARE pic		VARCHAR(5) DEFAULT LOWER(TRIM(NEW.PHOTO));

 SET addr1 = IF(IFNULL(addr1, '') = '', OLD.ADDRESSLINE1 , addr1);
 SET addr2 = IF(IFNULL(addr2, '') = '', OLD.ADDRESSLINE2 , addr2);
 SET town = IF(IFNULL(town, '') = '', OLD.CITY , town);
 SET nation = IF(IFNULL(nation, '') = '', OLD.COUNTRY , nation);
 SET geo = IF(IFNULL(geo, '') = '', OLD.GPS , geo);
 SET info = IF(IFNULL(info, '') = '', OLD.DESCRIPTION , info);
 SET tel = IF(IFNULL(tel, '') = '', OLD.TELEPHONE , tel);
 SET pic = IF(IFNULL(pic, '') = '', OLD.PHOTO , pic);

 SET NEW.ADDRESSLINE1 = IF(addr1 = '?', \N, addr1);
 SET NEW.ADDRESSLINE2 = IF(addr2 = '?', \N, addr2);
 SET NEW.CITY = IF(town = '?', \N, town);
 SET NEW.COUNTRY = IF(nation = '?', \N, nation);
 SET NEW.GPS = IF(geo = '?', \N, geo);
 SET NEW.DESCRIPTION = IF(info = '?', \N, info);
 SET NEW.TELEPHONE = IF(tel = '?', \N, tel);
 SET NEW.PHOTO = IF(pic = '?', \N, pic);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_insert_employee`
BEFORE UPDATE ON `employee`
FOR EACH ROW
BEGIN

 DECLARE wmail VARCHAR(254) DEFAULT LOWER(TRIM(NEW.EMAIL));
 DECLARE tel VARCHAR(15) DEFAULT TRIM(NEW.TELEPHONE);

 SET NEW.EMAIL = IF(wmail = '', \N, wmail);
 SET NEW.TELEPHONE = IF(tel = '', \N, tel);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_update_employee`
BEFORE UPDATE ON `employee`
FOR EACH ROW
BEGIN

 DECLARE wmail VARCHAR(254) DEFAULT LOWER(TRIM(NEW.EMAIL));
 DECLARE tel VARCHAR(15) DEFAULT TRIM(NEW.TELEPHONE);

 SET NEW.EMAIL = IF(wmail = '?', \N, wmail);
 SET NEW.TELEPHONE = IF(tel = '?', \N, tel);
 SET NEW.BRANCH_NO = IF(NEW.BRANCH_NO = 0, OLD.BRANCH_NO, NEW.BRANCH_NO);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_delete_employee`
BEFORE DELETE ON `employee`
FOR EACH ROW
BEGIN

 DECLARE org_name VARCHAR(45) DEFAULT(SELECT NAME FROM organization WHERE ORG_ID = OLD.ORG_ID);
 DECLARE active BOOLEAN DEFAULT (SELECT ACTIVE FROM user WHERE USER_ID = OLD.USER_ID);

 IF active THEN
  INSERT INTO notification(USER_ID, TYPE, ICON, MESSAGE) VALUES(OLD.USER_ID, 'event', 'fa fa-ban text-danger', CONCAT(org_name, ' administration REMOVED you as an Employee.'));
 END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_insert_logo`
BEFORE INSERT ON `logo`
FOR EACH ROW
BEGIN

 DECLARE id		BIGINT UNSIGNED DEFAULT tenant(NEW.ORG_ID);
 DECLARE format		VARCHAR(5) DEFAULT LOWER(TRIM(NEW.EXT));
 DECLARE info		VARCHAR(35) DEFAULT TRIM(NEW.DESCRIPTION);

 DECLARE FOUND BOOLEAN DEFAULT TRUE;
 DECLARE COUNT TINYINT UNSIGNED DEFAULT (SELECT COUNT(*) FROM logo WHERE ORG_ID = id);
 DECLARE INCR MEDIUMINT UNSIGNED DEFAULT 1;

 WHILE FOUND DO
  SET NEW.LOGO_NO = INCR;
  SET FOUND = ((SELECT COUNT(LOGO_NO) FROM logo WHERE LOGO_NO = NEW.LOGO_NO AND ORG_ID = id) > 0);
  SET INCR = INCR + 1;
 END WHILE;

 SET @LOGO_NO = NEW.LOGO_NO;
 SET @ORG_ID = id;
 SET NEW.ORG_ID = id;
 SET NEW.DESCRIPTION = IF(info = '', \N, info);
 SET NEW.EXT = IF(format = '', \N, format);
 SET NEW.MAIN = COUNT < 1;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_insert_notification`
BEFORE INSERT ON `notification`
FOR EACH ROW
BEGIN

 DECLARE chat		VARCHAR(255) DEFAULT TRIM(NEW.MESSAGE);
 DECLARE img		VARCHAR(32) DEFAULT TRIM(NEW.ICON);

 DECLARE FOUND BOOLEAN DEFAULT TRUE;
 DECLARE INCR MEDIUMINT UNSIGNED DEFAULT 1;

 WHILE FOUND DO
  SET NEW.NOT_COUNT = INCR;
  SET FOUND = ((SELECT COUNT(NOT_COUNT) FROM notification WHERE NOT_COUNT = NEW.NOT_COUNT AND USER_ID = NEW.USER_ID) > 0);
  SET INCR = INCR + 1;
 END WHILE;


 SET NEW.MESSAGE = IF(chat = '', \N, chat);
 SET NEW.ICON = IF(img = '', \N, img);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_insert_orginzation`
BEFORE INSERT ON `organization`
FOR EACH ROW
BEGIN

 DECLARE orgname VARCHAR(45) DEFAULT TRIM(NEW.NAME);
 DECLARE regno VARCHAR(21) DEFAULT TRIM(NEW.REGNO);
 DECLARE commission VARCHAR(85) DEFAULT TRIM(NEW.REGISTRA);
 DECLARE web VARCHAR(35) DEFAULT LOWER(TRIM(NEW.WEBSITE));
 DECLARE slogan VARCHAR(255) DEFAULT TRIM(NEW.MOTTO);

 SET NEW.NAME = IF(orgname = '', \N, orgname);
 SET NEW.REGNO = IF(regno = '', \N, regno);
 SET NEW.REGISTRA = IF(commission = '', \N, commission);
 SET NEW.WEBSITE = IF(web = '', \N, web);
 SET NEW.MOTTO = IF(slogan = '', \N, slogan);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_update_orginzation`
BEFORE UPDATE ON `organization`
FOR EACH ROW
BEGIN

 DECLARE commission VARCHAR(85) DEFAULT TRIM(NEW.REGISTRA);
 DECLARE dba VARCHAR(65) DEFAULT TRIM(NEW.DBA);
 DECLARE info VARCHAR(255) DEFAULT TRIM(NEW.DESCRIPTION);
 DECLARE orgname VARCHAR(45) DEFAULT TRIM(NEW.NAME);
 DECLARE regno VARCHAR(21) DEFAULT TRIM(NEW.REGNO);
 DECLARE slogan VARCHAR(255) DEFAULT TRIM(NEW.MOTTO);
 DECLARE struct CHAR(3) CHARSET UTF8 DEFAULT TRIM(NEW.STRUCT_ID);
 DECLARE web VARCHAR(35) DEFAULT LOWER(TRIM(NEW.WEBSITE));

 SET commission = IF(IFNULL(commission, '') = '', OLD.REGISTRA , commission);
 SET dba = IF(IFNULL(dba, '') = '', OLD.DBA, dba);
 SET info = IF(IFNULL(info, '') = '', OLD.DESCRIPTION, info);
 SET orgname = IF(IFNULL(orgname, '') = '', OLD.NAME , orgname);
 SET regno = IF(IFNULL(regno, '') = '', OLD.REGNO , regno);
 SET slogan = IF(IFNULL(slogan, '') = '', OLD.MOTTO, slogan);
 SET struct = IF(IFNULL(struct, '') = '', OLD.STRUCT_ID, struct);
 SET web = IF(IFNULL(web, '') = '', OLD.WEBSITE , web);

 SET NEW.DESCRIPTION = IF(info = '?', \N, info);
 SET NEW.DBA = IF(dba = '?', \N, dba);
 SET NEW.MOTTO = IF(slogan = '?', \N, slogan);
 SET NEW.NAME = IF(orgname = '?', OLD.NAME, orgname);
 SET NEW.REGNO = IF(regno = '?', \N, regno);
 SET NEW.STRUCT_ID = IF(struct = '?', \N, struct);
 SET NEW.REGISTRA = IF(commission = '?', \N, commission);
 SET NEW.WEBSITE = IF(web = '?', \N, web);

 SET NEW.MODIFIED = NOW();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_insert_payment`
BEFORE INSERT ON `paymentgateway`
FOR EACH ROW
BEGIN

 DECLARE processor CHAR(2) CHARSET UTF8 DEFAULT TRIM(NEW.PROVIDER_ID);
 DECLARE clientId VARCHAR(35) DEFAULT TRIM(NEW.CLIENT_ID);
 DECLARE access VARCHAR(255) DEFAULT TRIM(NEW.ACCESS_KEY);

 SET NEW.PROVIDER_ID = IF(processor = '', \N, processor);
 SET NEW.CLIENT_ID = IF(clientId = '', \N, clientId);
 SET NEW.ACCESS_KEY = IF(access = '', \N, access);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_update_payment`
BEFORE UPDATE ON `paymentgateway`
FOR EACH ROW
BEGIN

 DECLARE processor CHAR(2) CHARSET UTF8 DEFAULT TRIM(NEW.PROVIDER_ID);
 DECLARE clientId VARCHAR(35) DEFAULT TRIM(NEW.CLIENT_ID);
 DECLARE access VARCHAR(255) DEFAULT TRIM(NEW.ACCESS_KEY);

 SET processor = IF(IFNULL(processor, '') = '',  OLD.PROVIDER_ID, processor);
 SET clientId = IF(IFNULL(clientId, '') = '', OLD.CLIENT_ID, clientId);
 SET access = IF(IFNULL(access, '') = '', OLD.ACCESS_KEY, access);

 SET NEW.PROVIDER_ID = IF(processor = '?', \N, processor);
 SET NEW.CLIENT_ID = IF(clientId = '?', \N, clientId);
 SET NEW.ACCESS_KEY = IF(access = '?', \N, access);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_insert_user`
BEFORE INSERT ON `user`
FOR EACH ROW
BEGIN

 DECLARE fname VARCHAR(20) DEFAULT LOWER(TRIM(NEW.FIRSTNAME));
 DECLARE lname VARCHAR(20) DEFAULT LOWER(TRIM(NEW.LASTNAME));
 DECLARE umail VARCHAR(254) DEFAULT LOWER(TRIM(NEW.EMAIL));
 DECLARE idno VARCHAR(32) DEFAULT TRIM(NEW.IDNUMBER);
 DECLARE nation CHAR(2) DEFAULT UPPER(NEW.NATIONALITY);

 DECLARE FOUND BOOLEAN DEFAULT TRUE;
 DECLARE INCR MEDIUMINT UNSIGNED DEFAULT 1;
 
 WHILE FOUND DO
  SET NEW.USER_ID = CONCAT(nation,(SELECT (CONCAT(SUBSTRING(DATE_FORMAT(CURDATE(), '%Y'), 1, 1), DATE_FORMAT(CURDATE(), '%y')))), CASTE(NEW.IDNUMBER, NEW.NATIONALITY), LPAD(INCR, 6, '0'));
  SET FOUND = ((SELECT COUNT(USER_ID) FROM user WHERE USER_ID = NEW.USER_ID) > 0);
  SET INCR = INCR + 1;
 END WHILE;

 SET @USER_ID = NEW.USER_ID;

 SET NEW.FIRSTNAME = IF(fname = '', \N, fname);
 SET NEW.LASTNAME = IF(lname = '', \N, lname);
 SET NEW.EMAIL = IF(umail = '', \N, umail);
 SET NEW.IDNUMBER = IF(idno = '', \N, idno);
 SET NEW.NATIONALITY = IF(nation = '', \N, nation);
 SET NEW.DOB = dob(idno, nation, NEW.DOB);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_update_user`
BEFORE UPDATE ON `user`
FOR EACH ROW
BEGIN

 DECLARE fname VARCHAR(20) DEFAULT LOWER(TRIM(NEW.FIRSTNAME));
 DECLARE lname VARCHAR(20) DEFAULT LOWER(TRIM(NEW.LASTNAME));
 DECLARE idno VARCHAR(32) DEFAULT TRIM(NEW.IDNUMBER);
 DECLARE nation CHAR(2) DEFAULT UPPER(NEW.NATIONALITY);
 DECLARE uname VARCHAR(20) DEFAULT LOWER(TRIM(NEW.USERNAME));
 DECLARE umail VARCHAR(254) DEFAULT LOWER(TRIM(NEW.EMAIL));
 DECLARE uphone VARCHAR(15) DEFAULT TRIM(NEW.PHONE);
 DECLARE nickname VARCHAR(20) DEFAULT TRIM(NEW.ALIAS);
 DECLARE addr1 VARCHAR(45) DEFAULT TRIM(NEW.ADDRESSLINE1);
 DECLARE addr2 VARCHAR(45) DEFAULT TRIM(NEW.ADDRESSLINE2);
 DECLARE town VARCHAR(35) DEFAULT TRIM(NEW.CITY);
 DECLARE ctry CHAR(2) DEFAULT UPPER(NEW.COUNTRY);
 DECLARE pic CHAR(5) DEFAULT TRIM(NEW.AVATAR);

 SET fname = IF(IFNULL(fname, '') = '',  OLD.FIRSTNAME, fname);
 SET lname = IF(IFNULL(lname, '') = '', OLD.LASTNAME, lname);
 SET nickname = IF(IFNULL(nickname, '') = '', OLD.ALIAS, nickname);
 SET nation = IF(IFNULL(nation, '') = '', OLD.NATIONALITY, nation);
 SET idno = IF(IFNULL(idno, '') = '', OLD.IDNUMBER, idno);
 SET umail = IF(IFNULL(umail, '') = '', OLD.EMAIL, umail);
 SET pic = IF(IFNULL(pic, '') = '', OLD.AVATAR, pic);

 SET NEW.FIRSTNAME = IF(fname = '?', \N, fname);
 SET NEW.LASTNAME = IF(lname = '?', \N, lname);
 SET NEW.IDNUMBER = IF(idno = '?', OLD.IDNUMBER, idno);
 SET NEW.DOB = IF(NEW.DOB = CURRENT_DATE(), OLD.DOB, NEW.DOB);
 SET NEW.NATIONALITY = IF(nation = '?', \N, nation);
 SET NEW.EMAIL = IF(umail = '?', \N, umail);
 SET NEW.ADDRESSLINE1 = IF(addr1 = '?', \N, addr1);
 SET NEW.ADDRESSLINE2 = IF(addr2 = '?', \N, addr2);
 SET NEW.CITY = IF(town = '?', \N, town);
 SET NEW.COUNTRY = IF(ctry = '?', \N, ctry);
 SET NEW.USERNAME = IF(uname = '?', \N, uname);
 SET NEW.PHONE = IF(uphone = '?', \N, uphone);
 SET NEW.ALIAS = IF(nickname = '?', \N, nickname);
 SET NEW.AVATAR = IF(pic = '?', \N, pic);
 SET NEW.MODIFIED = NOW();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`strato`@`%`*/ /*!50003 TRIGGER `before_delete_user`
BEFORE DELETE ON `user`
FOR EACH ROW
BEGIN
	DELETE FROM employee WHERE USER_ID = OLD.USER_ID;
	SET @AVATAR = CONCAT(OLD.USER_ID,'.',OLD.AVATAR);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-11 14:31:17
