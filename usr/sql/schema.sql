-- MariaDB dump 10.19  Distrib 10.5.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: nishe
-- ------------------------------------------------------
-- Server version	10.5.13-MariaDB-0ubuntu0.21.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `nishe`
--

/*!40000 DROP DATABASE IF EXISTS `nishe`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `nishe` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `nishe`;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `ACTION_ID` char(4) NOT NULL,
  `NAME` varchar(16) NOT NULL,
  `DESCRIPTION` varchar(35) DEFAULT NULL,
  `RESOURCE_ID` char(2) NOT NULL,
  `PRIVATE` tinyint(1) NOT NULL DEFAULT 0,
  `METHOD` enum('get','post','delete','put','patch') NOT NULL DEFAULT 'get',
  `ROUTE` varchar(38) NOT NULL,
  `PREFLIGHT` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ACTION_ID`),
  UNIQUE KEY `NAME` (`NAME`,`RESOURCE_ID`),
  UNIQUE KEY `ROUTE` (`ROUTE`,`RESOURCE_ID`),
  KEY `RESOURCE_ID` (`RESOURCE_ID`),
  CONSTRAINT `action_ibfk_1` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `resource` (`RESOURCE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `CATEGORY_ID` char(2) NOT NULL,
  `NAME` varchar(22) NOT NULL,
  `ICON` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`CATEGORY_ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `industry`
--

DROP TABLE IF EXISTS `industry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `industry` (
  `NAME` varchar(28) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `ALT` varchar(20) DEFAULT NULL,
  `CATEGORY_ID` char(2) NOT NULL,
  `COUNT` tinyint(3) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`CATEGORY_ID`,`COUNT`),
  UNIQUE KEY `NAME` (`NAME`,`CATEGORY_ID`),
  CONSTRAINT `industry_ibfk_1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `category` (`CATEGORY_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `ACTION_ID` char(4) NOT NULL,
  `PROFILE_ID` char(1) NOT NULL,
  PRIMARY KEY (`ACTION_ID`,`PROFILE_ID`),
  KEY `PROFILE_ID` (`PROFILE_ID`),
  CONSTRAINT `permission_ibfk_1` FOREIGN KEY (`ACTION_ID`) REFERENCES `action` (`ACTION_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_ibfk_2` FOREIGN KEY (`PROFILE_ID`) REFERENCES `phiscal`.`profile` (`PROFILE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `RESOURCE_ID` char(2) NOT NULL,
  `NAME` varchar(16) NOT NULL,
  `DESCRIPTION` varchar(35) DEFAULT NULL,
  `HANDLER` varchar(28) NOT NULL,
  PRIMARY KEY (`RESOURCE_ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tenant_industry`
--

DROP TABLE IF EXISTS `tenant_industry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenant_industry` (
  `TENANT_ID` bigint(20) unsigned NOT NULL,
  `CATEGORY_ID` char(2) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `BANNER` varchar(5) DEFAULT NULL,
  `MAIN` tinyint(1) NOT NULL DEFAULT 0,
  `COUNT` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`TENANT_ID`,`CATEGORY_ID`,`COUNT`),
  KEY `CATEGORY_ID` (`CATEGORY_ID`,`COUNT`),
  CONSTRAINT `tenant_industry_ibfk_1` FOREIGN KEY (`TENANT_ID`) REFERENCES `phiscal`.`tenant` (`TENANT_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tenant_industry_ibfk_2` FOREIGN KEY (`CATEGORY_ID`, `COUNT`) REFERENCES `industry` (`CATEGORY_ID`, `COUNT`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trackrecord`
--

DROP TABLE IF EXISTS `trackrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trackrecord` (
  `RECORD_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(35) DEFAULT NULL,
  `TYPE` enum('picture','audio','video','text') NOT NULL,
  `FILE` varchar(5) NOT NULL,
  `ENTITY_ID` bigint(20) unsigned NOT NULL,
  `CATEGORY_ID` char(2) NOT NULL,
  `COUNT` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`RECORD_ID`),
  KEY `ENTITY_ID` (`ENTITY_ID`,`CATEGORY_ID`,`COUNT`),
  CONSTRAINT `trackrecord_ibfk_1` FOREIGN KEY (`ENTITY_ID`, `CATEGORY_ID`, `COUNT`) REFERENCES `tenant_industry` (`TENANT_ID`, `CATEGORY_ID`, `COUNT`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `v_profilePermission`
--

DROP TABLE IF EXISTS `v_profilePermission`;
/*!50001 DROP VIEW IF EXISTS `v_profilePermission`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_profilePermission` (
  `id` tinyint NOT NULL,
  `profile` tinyint NOT NULL,
  `resource` tinyint NOT NULL,
  `action` tinyint NOT NULL,
  `description` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `v_resourceAction`
--

DROP TABLE IF EXISTS `v_resourceAction`;
/*!50001 DROP VIEW IF EXISTS `v_resourceAction`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_resourceAction` (
  `resource` tinyint NOT NULL,
  `method` tinyint NOT NULL,
  `route` tinyint NOT NULL,
  `action` tinyint NOT NULL,
  `preflight` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `private` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'nishe'
--
/*!50003 DROP FUNCTION IF EXISTS `banner` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` FUNCTION `banner`(target_tenant BIGINT UNSIGNED,
	industry_category CHAR(2) CHARSET UTF8,
	industry_count TINYINT UNSIGNED
) RETURNS text CHARSET utf8mb4
BEGIN
	DECLARE file VARCHAR(5) DEFAULT(SELECT BANNER FROM tenant_industry WHERE TENANT_ID = target_tenant AND CATEGORY_ID = industry_category AND COUNT = industry_count);
	DECLARE mainCategory CHAR(2) CHARSET UTF8 DEFAULT(SELECT CATEGORY_ID FROM tenant_industry WHERE TENANT_ID = target_tenant AND MAIN);
	DECLARE mainCount TINYINT UNSIGNED DEFAULT(SELECT COUNT FROM tenant_industry WHERE tenant_ID = target_tenant AND MAIN);
	DECLARE primeFile VARCHAR(5) CHARSET UTF8 DEFAULT(SELECT BANNER FROM tenant_industry WHERE TENANT_ID = target_tenant AND MAIN);

	IF file IS \N THEN RETURN \N; END IF;

	IF file = 'main' THEN RETURN CONCAT(mainCategory,mainCount,'.',primeFile); END IF;

	RETURN CONCAT(industry_category,industry_count,'.',file);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `associate_industry` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`strato`@`%` PROCEDURE `associate_industry`(
	IN tenantId		BIGINT UNSIGNED,
	IN categoryId		CHAR(2) CHARSET UTF8,
	IN industryCount	TINYINT UNSIGNED,
	IN prime		BOOLEAN,
	IN intel 		VARCHAR(255),
	IN marketing 		VARCHAR(5),
	OUT code 		INT,
	OUT info 		VARCHAR(255)
)
BEGIN
	DECLARE EXIT HANDLER FOR SQLEXCEPTION,1265
	BEGIN
		GET DIAGNOSTICS CONDITION 1 code = MYSQL_ERRNO, info = MESSAGE_TEXT;
		ROLLBACK;
	END;
	DECLARE EXIT HANDLER FOR 1062
	BEGIN
		UPDATE tenant_industry SET
				DESCRIPTION = IF(TRIM(intel) = '', '?', intel), 
				BANNER = marketing, 
				MAIN = prime
			WHERE TENANT_ID = tenantId 
			AND CATEGORY_ID = categoryId 
			AND COUNT = industryCount;

		SELECT banner(e.tenant_id, e.category_id, e.count) file, banner, e.tenant_id, e.category_id, e.count, i.name, e.description, e.main
			FROM industry i
			INNER JOIN tenant_industry e
			USING (category_id, count)
			WHERE tenant_id = tenantId;
		COMMIT;
		SET info = tenantId;
		SET code = 0;
	END;
	START TRANSACTION;
		IF prime = TRUE THEN UPDATE tenant_industry SET MAIN = FALSE; END IF;

		INSERT INTO tenant_industry VALUES(tenantId, categoryId, intel, marketing, prime, industryCount);
		SELECT banner(e.tenant_id, e.category_id, e.count) file, banner, e.tenant_id, e.category_id, e.count, i.name, e.description, e.main
			FROM industry i
			INNER JOIN tenant_industry e
			USING (category_id, count)
			WHERE tenant_id = tenantId;
	COMMIT;
	SET info = tenantId;
	SET code = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Current Database: `nishe`
--

USE `nishe`;

--
-- Final view structure for view `v_profilePermission`
--

/*!50001 DROP TABLE IF EXISTS `v_profilePermission`*/;
/*!50001 DROP VIEW IF EXISTS `v_profilePermission`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_profilePermission` AS select `p`.`PROFILE_ID` AS `id`,`p`.`NAME` AS `profile`,`r`.`NAME` AS `resource`,`a`.`ROUTE` AS `action`,`a`.`DESCRIPTION` AS `description` from (((`phiscal`.`profile` `p` join `nishe`.`permission` `m` on(`p`.`PROFILE_ID` = `m`.`PROFILE_ID`)) join `nishe`.`action` `a` on(`m`.`ACTION_ID` = `a`.`ACTION_ID`)) join `nishe`.`resource` `r` on(`a`.`RESOURCE_ID` = `r`.`RESOURCE_ID`)) order by `p`.`NAME`,`r`.`NAME`,`a`.`ROUTE` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_resourceAction`
--

/*!50001 DROP TABLE IF EXISTS `v_resourceAction`*/;
/*!50001 DROP VIEW IF EXISTS `v_resourceAction`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`strato`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_resourceAction` AS select `r`.`NAME` AS `resource`,`a`.`METHOD` AS `method`,`a`.`ROUTE` AS `route`,`a`.`NAME` AS `action`,`a`.`PREFLIGHT` AS `preflight`,`a`.`DESCRIPTION` AS `description`,`a`.`PRIVATE` AS `private` from (`action` `a` join `resource` `r` on(`a`.`RESOURCE_ID` = `r`.`RESOURCE_ID`)) order by `r`.`NAME`,`a`.`ROUTE` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-11 14:30:48
