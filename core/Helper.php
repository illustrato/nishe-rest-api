<?php
declare(strict_types=1);

namespace Nishe\Core;

use Phalcon\Di;
use Phalcon\Di\DiInterface;

if (true !== function_exists('Nishe\Core\container')) {
    /**
     * Call Dependency Injection container
     *
     * @return mixed|null|DiInterface
     */
    function container()
    {
        $default = Di::getDefault();
        $args = func_get_args();
        if (empty($args)) {
            return $default;
        }

        return call_user_func_array([$default, 'get'], $args);
    }
}

if (true !== function_exists('Nishe\Core\appPath')) {
    /**
     * Get projects relative root path
     *
     * @param string $path
     *
     * @return string
     */
    function appPath(string $path = ''): string
    {
        return realpath('.') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if (true !== function_exists('Nishe\Core\envValue')) {
    /**
     * Gets a variable from the environment, returns it properly formatted or the
     * default if it does not exist
     *
     * @param string		$variable
     * @param mixed|null	$default
     *
     * @return mixed
     */
    function envValue(string $variable, $default = null)
    {
        $return = $default;
        $value = getenv($variable);
        $values = [
            'false' => false,
            'true' => true,
            'null' => null,
        ];
        if (false !== $value) {
            $return = $values[$value] ?? $value;
        }
        return $return;
    }
}

if (true !== function_exists('Nishe\Core\boolProof')) {
    /**
     * Converts worded boolean values to bit equivalents
     * allowing the third option
     *
     * @param mixed		$variable
     * @param mixed|int		$default
     *
     * @return mixed
     */
    function boolProof($variable, $default = -1)
    {
        $values = [
            false => 0,
            true => 1,
            $default => $default,
        ];
        return $values[$variable];
    }
}

if (true !== function_exists('Nishe\Core\rmR')) {
    /**
     * Get projects relative root path
     *
     * @param string	$path	: Target Directory with Content to delete
     * @param bool	$self	: Delete Target Directory too
     *
     * @return string
     */
    function rmR(string $path, bool $self = true)
    {
        if (!is_dir($path)) {
            return true;
        }
        $files = array_diff(scandir($path), ['.', '..']);
        foreach ($files as $file) {
            is_dir("$path/$file") ? rmR("$path/$file") : unlink("$path/$file");
        }
        if ($self) {
            return rmdir($path);
        }
        return true;
    }
}
