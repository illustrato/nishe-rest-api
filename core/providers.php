<?php
return [
    \Nishe\Provider\Config::class,
    \Nishe\Provider\Database::class,
    \Nishe\Provider\Http::class,
    \Nishe\Provider\Identity::class,
    \Nishe\Provider\Router::class,
    \Nishe\Provider\Session::class,
    \Nishe\Provider\Url::class,
];
?>
