<?php
use Phalcon\Loader;

use function Nishe\Core\appPath;

// Regoster Auto Loader
require_once __DIR__ . '/Helper.php';

$loader = new Loader();
$loader->registerNamespaces([
    'Nishe\Model' => appPath('/src/model'),
    'Nishe\Model\Entity' => appPath('/src/model/entity'),

    'Nishe\Controller' => appPath('/src/controller'),
    'Nishe\Provider' => appPath('/src/provider'),
    'Nishe\Middleware' => appPath('/src/middleware'),
    'Nishe\Traits' => appPath('/src/trait'),

    'Nishe\Plugin' => appPath('/src/plugin'),
    'Nishe\Plugin\Http' => appPath('/src/plugin/http'),
    'Nishe\Plugin\Identity' => appPath('/src/plugin/identity'),
]);
$loader->registerClasses(['Nishe\Application' => appPath('/src/Application.php')]);
$loader->registerFiles([appPath('/core/Helper.php')]);
$loader->register();

/**
 * Composer Autoloader
 */
require_once appPath('/vendor/autoload.php');
?>
